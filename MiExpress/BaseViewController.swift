//
//  BaseViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 24/11/16.
//  Copyright © 2016 JASolutions. All rights reserved.
//

import UIKit
import SystemConfiguration


class BaseViewController : UIViewController, UIAlertViewDelegate{
    
    var activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        //connectToFcm();
    }
    
//    func connectToFcm() {
//        FIRMessaging.messaging().connect { (error) in
//            if (error != nil) {
//                print("Unable to connect with FCM. \(error)")
//            } else {
//                print("Connected to FCM.")
//            }
//        }
//    }
    
    public func setCustomNavigationBar(title:String){
        self.navigationController?.navigationBar.barTintColor = COLOR_SHARP_BLUE
        self.navigationItem.title = title;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    public func setCustomBackground(){
        self.view.backgroundColor = COLOR_GRAY
    }
    
    public func setCutsomNavigationBarImage(imageTitle: String){
        let image = #imageLiteral(resourceName: "logo_titulo copia")
        navigationItem.titleView = UIImageView(image: image)
    }
    
    public func hiddenButtonBarLeft(hidden:Bool){
        self.navigationItem.setHidesBackButton(hidden, animated: false);
    }
    
    public func setIconBackButton(){
        let yourBackImage = #imageLiteral(resourceName: "flecha_izq copia")
        self.navigationController?.navigationBar.tintColor = UIColor(red: 200/250, green: 200/250, blue: 200/250, alpha: 1.0)
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
    }
    
    public func setBorderBottomTextField(textField:UITextField){
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = COLOR_GRAY_TEXTFIELD
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }
    
    public func showMessageWith(text: String, headerText: String, actionSegue: String){
        let alert = UIAlertController(title: headerText, message: text, preferredStyle: UIAlertControllerStyle.alert)
        let OKAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            
            if actionSegue != ""{
                if self.navigationController != nil {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: actionSegue, sender: nil)
                    }
                }
            }
        }
        
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    public func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    public func startActivity(){
        self.hiddenButtonBarLeft(hidden: true)
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.activityIndicator.center = view.center
        self.activityIndicator.layer.backgroundColor = COLOR_TRANSPARENT.cgColor
        self.activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }
    
    public func stopActivity(){
        self.hiddenButtonBarLeft(hidden: false)
        self.activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
    }
    
    //Conection WIFI
    
    public func isInternetAvailable() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
    
    func nextGenericController(identifier: String){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(productController, animated: true)
    }
    
}

extension UITextField {
    
    func setBottomBorderTextField(color:CGColor) {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0).cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = color
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    func setError(){
        self.layer.borderColor = UIColor.red.cgColor

    }
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: -10, y: self.frame.height - 10, width: self.frame.width + 20, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}









