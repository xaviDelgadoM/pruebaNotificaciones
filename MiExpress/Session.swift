//
//  Session.swift
//  MiExpress
//
//  Created by Jacob Velarde on 19/02/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

public class Session {
    
    static let sharedInstance = Session()
    private init() {}
    
    var informationShop = [[String: String]]()
    var sessionEmail = String()
    var totalPay = String()
    var folio = String()


    public func userSave(email: String){
        sessionEmail = email
    }
    
    public func getUserEmail()->String{
        
        if !sessionEmail.isEmpty{
            return sessionEmail
        }else{
            return EMAIL_NOT_FOUND
        }
    }
    
    public func saveUser(email: String, pass: String){
        UserDefaults.standard.setValue(email, forKey: "user_mail")
        UserDefaults.standard.setValue(pass, forKey: "user_pass")
    }
    
    public func getInformationShop()->[[String: String]]{
        return informationShop
    }
    
    public func saveInformationShop(dataShop : [String: String]){
        informationShop.append(dataShop)
    }
    
    public func removeProduct(index: Int){
        informationShop.remove(at: index)
    }
    
    public func removeAllProductAndPay(){
        informationShop.removeAll()
        totalPay = ""
        folio = ""
    }
    
    public func setTotalPay(total: String){
        totalPay = total
    }
    
    public func getTotalPay()->String{
        return totalPay
    }
    
    public func closeSession(){
        informationShop.removeAll()
        sessionEmail = ""
        totalPay = ""
        folio = ""
    }
    
    public func saveFolio(aFolio : String){
        folio = aFolio;
    }
    
    public func getFolio()->String{
        
        return folio;
    }
    
}
