//
//  CreateAccountViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 28/12/16.
//  Copyright © 2016 JASolutions. All rights reserved.
//

import UIKit

class CreateAccountViewController: BaseViewController {
    
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textRePassword: UITextField!
    @IBOutlet weak var textPhone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad();

        self.setCustomNavigationBar(title: TITLE_CREATE_ACCOUNT)
        self.setIconBackButton()
        self.setCustomBackground()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == SEGUE_HOME_VIEW_CONTROLLER_REGISTER) {
            if validateForm(){
                if self.isInternetAvailable(){
                    self.startActivity()
                    let createAccount = GenericDelegateResponse();
                    createAccount.makeRequestCreateAccountWithUser(name: textName.text!, email: textEmail.text!, pass: textPassword.text!, phone: textPhone.text!, ClassDelegate: self)
                }else{
                    self.showMessageWith(text: INTERNET_NOT_AVAILABLE, headerText: "Aviso", actionSegue: "")
                }

            }else{
            
                return false
            }
        }else{
            return true
        }
        return false
    }
    
//    private func setCustomTextField(){
//        textName.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//        textEmail.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//        textPassword.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//        textRePassword.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//    }
    
    private func validateForm() -> Bool{
        let name = textName.text;
        let email = textEmail.text;
        let pass = textPassword.text;
        let rePass = textRePassword.text;
        let phone = textPhone.text;
        
        if(name == nil || name == ""){
            //textName.setBottomBorderTextField(color: UIColor.red.cgColor)
            textName.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa un nombre", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        if (email == nil || email == "" || !isValidEmail(testStr: email!)) {
            textEmail.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa un email valido", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        if (pass == nil || pass == ""){
            textPassword.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa una contraseña", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        if (rePass == nil || rePass == ""){
            textRePassword.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Repite tu contraseña", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        if (pass != rePass){
            textPassword.backgroundColor = COLOR_LOW_ORANGE
            textRePassword.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Las contraseñas no son iguales", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        if (phone == nil || phone == ""){
            textPhone.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingrese un telefono", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        if (phone?.characters.count != 10) {
            textPhone.backgroundColor = COLOR_LOW_ORANGE;
            self.showMessageWith(text: "Ingresa un telefono valido", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        return true;
    }
    
}

extension CreateAccountViewController: GenericResponse{
    
    func delegateGenericResponse(codeError: String, message: String, object: String, data: Data) {
        if codeError == EXITO {
            self.stopActivity()
            DispatchQueue.main.async {
                self.showMessageWith(text: message, headerText: "Aviso", actionSegue: "")
            }
            self.performSegue(withIdentifier: "CancelCreateAccount", sender: nil)
            
        }else{
            self.stopActivity()
            self.showMessageWith(text: object, headerText: message, actionSegue: "")
        }
    }
}








