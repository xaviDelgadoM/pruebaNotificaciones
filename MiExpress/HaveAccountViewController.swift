//
//  HaveAccount.swift
//  MiExpress
//
//  Created by Jacob Velarde on 04/01/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

class HaveAccountViewController:BaseViewController{
    
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCutsomNavigationBarImage(imageTitle: "logo_titulo copia")
        self.setIconBackButton()
        
        if((UserDefaults.standard.value(forKey: "user_mail")) != nil && (UserDefaults.standard.value(forKey: "user_pass")) != nil){
            self.startActivity();
            textEmail.text = (UserDefaults.standard.value(forKey: "user_mail") as! String?)
            textPassword.text = (UserDefaults.standard.value(forKey: "user_pass") as! String?)
            let makeLogin = GenericDelegateResponse()
            makeLogin.makeRequestLoginWithUser(email: (UserDefaults.standard.value(forKey: "user_mail") as! String), password: (UserDefaults.standard.value(forKey: "user_pass") as! String), ClassDelegate: self)
            
            //self.nextGenericController(identifier: SEGUE_HOME_VIEW_CONTROLLER)

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == SEGUE_HOME_VIEW_CONTROLLER_LOGIN) {
            if isValidForm(){
                //Optional
                //Session.sharedInstance.saveUser(email: textEmail.text!, pass: textPassword.text!)
                //Session.sharedInstance.userSave(email: textEmail.text!)
                //Optional

                //return true
                self.startActivity()
                let makeLogin = GenericDelegateResponse()
                makeLogin.makeRequestLoginWithUser(email: textEmail.text!, password: textPassword.text!, ClassDelegate: self)
            }else{
                return false
            }
        }
        
        return false
    }
    
    
    private func isValidForm()->Bool{
        let email = textEmail.text
        let pass = textPassword.text
        
        if (email == nil || email == "" || !isValidEmail(testStr: email!)) {
            textEmail.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa un email valido", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        if (pass == nil || pass == ""){
            textPassword.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa una contraseña", headerText: "Aviso", actionSegue: "")
            return false;
        }
        
        return true;
    }
}

extension HaveAccountViewController: GenericResponse{
    
    func delegateGenericResponse(codeError: String, message: String, object: String, data: Data) {
        print(codeError)
        print(message)
        print(object)
        
        if codeError == EXITO {
            self.stopActivity()
            Session.sharedInstance.saveUser(email: textEmail.text!, pass: textPassword.text!)
            Session.sharedInstance.userSave(email: textEmail.text!)
            self.nextGenericController(identifier: SEGUE_HOME_VIEW_CONTROLLER)
        }else{
            self.stopActivity()
            self.showMessageWith(text: object, headerText: message, actionSegue: "")
        }
    }
    
}




