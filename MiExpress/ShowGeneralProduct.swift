//
//  ShowGeneralProduct.swift
//  MiExpress
//
//  Created by Jacob Velarde on 23/02/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

class ShowGeneralProduct: BaseViewController{
    
    public var arrayData = [String: String]()
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var titleProduct: UILabel!
    @IBOutlet weak var descriptionProduct: UITextView!
    @IBOutlet weak var priceProduct: UILabel!
    @IBOutlet weak var cantidadProduct: UILabel!
    @IBOutlet weak var btnLess: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnAddShop: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomNavigationBar(title: TITLE_DETAIL)
        self.setData()
        
    }
    
    private func setData(){
        
        let url : NSString = arrayData["imgUrl"]! as NSString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        let urlImage = URL(string: urlStr as String)
        
        imageProduct.downloadedFrom(url: urlImage!)
        titleProduct.text = arrayData["nombre"]
        descriptionProduct.text = arrayData["descripcion"]
        priceProduct.text = "$ " + arrayData["price"]!
        descriptionProduct.isEditable = false
        
    }

    
    @IBAction func btnBuy(_ sender: Any) {
        
        //for let i = 0; i<Int(cantidadProduct.text!)!;{
        let total = Int(cantidadProduct.text!)
        for _ in 0 ..< total! {
            Session.sharedInstance.saveInformationShop(dataShop: arrayData)
        }
        
        let session = Session.sharedInstance.getInformationShop()
        
        if !session.isEmpty {
            _ = self.navigationController?.popViewController(animated: true)
        }else{
            //self.showMessageWith(text: "Error al guardar", headerText: "¡Aviso!", actionSegue: "")
        }
    }
    
    @IBAction func btnLessProduct(_ sender: Any) {
        
        let cantidad = Int(cantidadProduct.text!)
        
        if cantidad! > 0 {
            cantidadProduct.text = String(cantidad! - 1)
        }
    
    }
    
    @IBAction func btnMoreProduct(_ sender: Any) {
        let cantidad = Int(cantidadProduct.text!)
        cantidadProduct.text = String(cantidad! + 1)

    }
    
    @IBAction func btnLessProductDown(_ sender: Any) {
        btnLess.imageView?.image = #imageLiteral(resourceName: "btn_menos_activo copia")
    }
    
    @IBAction func btnMoreProductDown(_ sender: Any) {
        btnMore.imageView?.image = #imageLiteral(resourceName: "btn_mas_activo copia")
    }

    @IBAction func btnAddShopDown(_ sender: Any) {
        btnAddShop.imageView?.image = #imageLiteral(resourceName: "btn_agregar_on copia")
    }
    
}




