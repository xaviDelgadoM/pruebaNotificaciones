//
//  ProductViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 26/01/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProductViewController: UITableViewController {
    
    @IBOutlet var tableViewProduct: UITableView!
    var typeProduct = String()
    var objectData = Data()
    var arrayData = [[String: String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = COLOR_SHARP_BLUE
        self.setCustomNavigationBar(title: TITLE_LIST_PRODUCT)
        
        parseData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CustomCell = self.tableViewProduct.dequeueReusableCell(withIdentifier: CUSTOM_CELL_PRODUCT_IDENTIFIER) as! CustomCell
        
        let information = arrayData[indexPath.row]
        
        let url : NSString = information["imgUrl"]! as NSString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        let urlImage = URL(string: urlStr as String)

        cell.imageProduct.downloadedFrom(url: urlImage!)
        cell.imageProduct.layer.cornerRadius = 10
        cell.tittle.text = information["nombre"]
        cell.descriptionProduct.text = information["descripcion"]

        //View with shadow
//        cell.container.layer.shadowColor = UIColor.black.cgColor
//        cell.container.layer.shadowOpacity = 1
//        cell.container.layer.shadowOffset = CGSize(width: 0, height: 3)
//        cell.container.layer.shadowRadius = 1
        cell.container.layer.cornerRadius = 10
        cell.backgroundColor = COLOR_SHARP_BLUE
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selecciono \(indexPath.row)")
        let dataProduct = arrayData[indexPath.row]
        showNextViewControllerWith(identifier: SEGUE_GENERAL_PRODUCT, dataP: dataProduct)
        
    }
    
    func parseData(){
        let jsonResponse = JSON(data: objectData)
        let dataParse = jsonResponse["respuesta"]["object"].stringValue
        
        if let dataFromString = dataParse.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            print(json)
            
            for result in json.arrayValue {
                let idProduct = result["idProducto"].stringValue
                let nombre = result["nombre"].stringValue
                let descripcion = result["descripcion"].stringValue
                let imgUrl = returnStringURL(stringUrl: result["imgUrl"].stringValue)
                let price = result["precio"].stringValue
                let obj = ["nombre": nombre, "descripcion": descripcion, "imgUrl": imgUrl, "price": price, "idProducto": idProduct]
                arrayData.append(obj)
                print(arrayData)
            }
        }
    }
    
    private func returnStringURL(stringUrl: String)->String{
        //let startIndex = stringUrl.index(stringUrl.startIndex, offsetBy: 14)
        //print("ImagesURL \(URL_SERVER_IMAGES+"/"+stringUrl.substring(from: startIndex))")
        return URL_SERVER_IMAGES+stringUrl
    }
    
    func showNextViewControllerWith(identifier: String, dataP: [String: String]){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: identifier) as! ShowGeneralProduct
        productController.arrayData = dataP
        self.navigationController?.pushViewController(productController, animated: true)
   
    }
    
    public func setCustomNavigationBar(title:String){
        self.navigationController?.navigationBar.barTintColor = COLOR_SHARP_BLUE
        self.navigationItem.title = title;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
}
