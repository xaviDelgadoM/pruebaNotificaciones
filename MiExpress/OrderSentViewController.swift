//
//  OrderSentViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 12/01/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

class OrderSentViewController : BaseViewController{
    
    
    @IBOutlet weak var textEmail: UILabel!
    @IBOutlet weak var textFolio: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hiddenButtonBarLeft(hidden: true)
        textEmail.text = "Correo: " + Session.sharedInstance.getUserEmail()
        textFolio.text = "Folio: " + Session.sharedInstance.getFolio()
        Session.sharedInstance.removeAllProductAndPay()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if (identifier == SEGUE_HOME_VIEW_CONTROLLER_FINISH) {
            self.navigationItem.title = ""
        }
    }
}
