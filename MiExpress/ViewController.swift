//
//  ViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 24/11/16.
//  Copyright © 2016 JASolutions. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
        @IBOutlet weak var prueba: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.setCustomNavigationBar(title: "")
        Session.sharedInstance.closeSession()
        self.hiddenButtonBarLeft(hidden: true)        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

