//
//  DeliveryAddress.swift
//  MiExpress
//
//  Created by Jacob Velarde on 04/01/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

class DeliveryAddressViewController: BaseViewController{
    
    @IBOutlet weak var textStreet: UITextField!
    @IBOutlet weak var textNumInt: UITextField!
    @IBOutlet weak var textNumExt: UITextField!
    @IBOutlet weak var textColony: UITextField!
    @IBOutlet weak var textMunicipio: UITextField!
    @IBOutlet weak var textPostalCode: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCustomNavigationBar(title: TITLE_DELIVERY_ADDRESS)
        self.hiddenButtonBarLeft(hidden: true)
        //setCustomTextField()
        self.setCustomBackground()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    private func setCustomTextField(){
//        textStreet.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//        textNumInt.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//        textNumExt.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//        textColony.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//        textCity.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//        textPostalCode.setBottomBorderTextField(color: COLOR_GRAY_TEXTFIELD)
//    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == SEGUE_HOME_VIEW_CONTROLLER_REGISTER){
            if validateForm(){
                return true
            }else{
                return false;
            }
        }
        
        return true
    }
    
    private func validateForm() -> Bool{
        let street = textStreet.text
        let num = textNumExt.text
        let colony = textColony.text
        let city = textMunicipio.text
        let cp = textPostalCode.text
        
        if (street == nil || street == "") {
            textStreet.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingrese una calle", headerText: "¡Alerta!", actionSegue: "")
            return false
        }
        
        if(num == nil || num == ""){
            textNumExt.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el número de casa exterior", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (colony == nil || colony == ""){
            textColony.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa la colonia", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (city == nil || city == ""){
            textMunicipio.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el municipio", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (cp == nil || cp == ""){
            textPostalCode.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el código postal", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (cp?.characters.count != 5){
            textPostalCode.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el código postal correcto", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        return true;
    }
}











