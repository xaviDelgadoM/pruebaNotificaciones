//
//  HomeViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 30/11/16.
//  Copyright © 2016 JASolutions. All rights reserved.
//

import UIKit


class HomeViewController: BaseViewController {
    
    var newBackButton = UIBarButtonItem();
    var shopButton = UIBarButtonItem();
    
    enum type:String {
        case Food = "comida"
        case Gifts = "regalos"
        case Drinks = "bebidas"
        case SuperMarket = "superMercado"
        case Pharmacy = "farmacia"
        case Default = "Default"
    }
    
    var SelectType = type.Default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCustomNavigationBar(title: TITLE_HOME);
        newBackButton = UIBarButtonItem(title: "Salir", style: UIBarButtonItemStyle.plain, target: self, action:#selector(HomeViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        shopButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icn_carrito_on copia") , style: .plain, target: self, action: #selector(addShop))
        self.navigationItem.rightBarButtonItem = shopButton

        self.setCustomBackground()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back(sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title =  TITLE_HOME

    }
    
    func addShop(sender: UIBarButtonItem){
        self.navigationItem.title = ""
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let confirmShopViewController = storyBoard.instantiateViewController(withIdentifier: "ConfirmShopViewController") as! ConfirmShopViewController
        self.navigationController?.pushViewController(confirmShopViewController, animated: true)
    }
    
    
    @IBAction func getFood(_ sender: Any) {
        SelectType = .Food
        self.makeRequestWith(typeProduct: SelectType.rawValue)
        //self.nextStoreViewController(/*data: nil*/)
        //self.showMessageWith(text: "Productos no disponibles", headerText: "Aviso", actionSegue: "")
    }
    
    @IBAction func getGifts(_ sender: Any) {
        SelectType = .Gifts
        //self.makeRequestWith(typeProduct: SelectType.rawValue)

    }
    
    @IBAction func getDrinks(_ sender: Any) {
        SelectType = .Drinks
        //self.makeRequestWith(typeProduct: SelectType.rawValue)

    }
    
    @IBAction func getSuperMarket(_ sender: Any) {
        SelectType = .Drinks
        self.makeRequestWith(typeProduct: SelectType.rawValue)
        //SelectType = .SuperMarket
        //self.makeRequestWith(typeProduct: SelectType.rawValue)
        //self.showMessageWith(text: "Productos no disponibles", headerText: "Aviso", actionSegue: "")
    }
    
    @IBAction func getPharmacy(_ sender: Any) {
        SelectType = .Pharmacy
        //self.makeRequestWith(typeProduct: SelectType.rawValue)

    }
    
    @IBAction func getTaxi(_ sender: Any) {
        
        
    }
    
    
    func makeRequestWith(typeProduct: String){
        if self.isInternetAvailable() {
            self.startActivity()
            newBackButton.isEnabled = false
            shopButton.isEnabled = false
            let getStore = GenericDelegateResponse()
            
            if typeProduct == "comida"{
                getStore.makeRequestPutStore(category: "1", ClassDelegate: self);
                
            }else if typeProduct == "bebidas"{
                getStore.makeRequestPutStore(category: "3", ClassDelegate: self);
                
            }else{
                self.showMessageWith(text: "Categoría no disponible", headerText: "Aviso", actionSegue: "")
            }
            
            //getProducts.makeRequestGetProductsWithType(type: typeProduct, ClassDelegate: self)
        }else{
            self.showMessageWith(text: INTERNET_NOT_AVAILABLE, headerText: "Aviso", actionSegue: "")
        }
    }

    func nextProductViewController(type: String, data: Data){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: SEGUE_PRODUCT_CONTROLLER) as! ProductViewController
        productController.typeProduct = SelectType.rawValue
        productController.objectData = data
        self.navigationController?.pushViewController(productController, animated: true)
    }
    
    func nextStoreViewController(data: Data){
        let storyBoard = UIStoryboard(name:"Main", bundle:nil)
        let storeController = storyBoard.instantiateViewController(withIdentifier: SEGUE_STORE_CONTROLLER) as! StoresViewController
        storeController.objectData = data
        self.navigationController?.pushViewController(storeController, animated: true)
    }
    
    func resetUIBarButtons(){
        newBackButton = UIBarButtonItem(title: "Salir", style: UIBarButtonItemStyle.plain, target: self, action:#selector(HomeViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        shopButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icn_carrito_on copia") , style: .plain, target: self, action: #selector(addShop))
        self.navigationItem.rightBarButtonItem = shopButton
        
        newBackButton.isEnabled = true
        shopButton.isEnabled = true
    }
}

extension HomeViewController: GenericResponse{
    
    func delegateGenericResponse(codeError: String, message: String, object: String, data: Data) {
        print(codeError)
        print(message)
        print(object)
        
        self.resetUIBarButtons()
    
        if codeError == EXITO {
            self.stopActivity()

            switch SelectType {
            
            case .Default:
                break;
            
            case .Food:
                //self.nextProductViewController(type: SelectType.rawValue, data: data)
                self.nextStoreViewController(data: data)
                break;
            
            case .Gifts:
                self.nextProductViewController(type: SelectType.rawValue, data: data)
                break;
            
            case .Drinks:
                //self.nextProductViewController(type: SelectType.rawValue, data: data)
                self.nextStoreViewController(data: data)
                break;
            
            case .SuperMarket:
                //self.nextProductViewController(type: SelectType.rawValue, data: data)
                //self.nextStoreViewController(data: data)
                break;
            
            case .Pharmacy:
                self.nextProductViewController(type: SelectType.rawValue, data: data)
                break;
            
            }
        }else{
            self.stopActivity()
            self.showMessageWith(text: object, headerText: message, actionSegue: "")
        }
        
    }
    
}


