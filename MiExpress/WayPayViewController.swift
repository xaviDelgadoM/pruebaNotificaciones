//
//  WayPay.swift
//  MiExpress
//
//  Created by Jacob Velarde on 12/01/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

class WayPayViewController : BaseViewController{
    
    @IBOutlet weak var textTipo: UITextField!
    @IBOutlet weak var textNumTarjeta: UITextField!
    @IBOutlet weak var textTitularTarjeta: UITextField!
    @IBOutlet weak var textCodigoSeguridad: UITextField!
    @IBOutlet weak var tipoPagoEfectivo: UIImageView!
    @IBOutlet weak var btnSaveDataOutlet: UIButton!
    @IBOutlet weak var labelTotalPago: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCustomNavigationBar(title: TITLE_WAY_PAY);
        tipoPagoEfectivo.isHidden = true
        btnSaveDataOutlet.isHidden = true
        labelTotalPago.numberOfLines = 0
        labelTotalPago.sizeToFit()
        labelTotalPago.text = "$" + Session.sharedInstance.getTotalPay()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = TITLE_WAY_PAY
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if (identifier == SEGUE_DELIVERY_ADDRESS_PAY_VIEW_CONTROLLER) {
            self.navigationItem.title = ""
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == SEGUE_DELIVERY_ADDRESS_PAY_VIEW_CONTROLLER){
            //if validateForm(){
                self.navigationItem.title = ""
            //    return true
            //}else{
                return true
            //}
        }
        
        return true
    }
    
    @IBAction func btnSaveData(_ sender: Any) {
        
        btnSaveDataOutlet.setImage(#imageLiteral(resourceName: "check_on copia"), for: UIControlState.normal)

    }
    
    
    private func validateForm() -> Bool{
        let tipo = textTipo.text
        let numTarjeta = textNumTarjeta.text
        let titular = textTitularTarjeta.text
        let codigoSeguridad = textCodigoSeguridad.text
        
        if (tipo == nil || tipo == ""){
            textTipo.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el tipo de tarjeta", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (numTarjeta == nil || numTarjeta == ""){
            textNumTarjeta.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el número de tarjeta", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (titular == nil || titular == ""){
            textTitularTarjeta.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el nombre del titular de la tarjeta", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (codigoSeguridad == nil || codigoSeguridad == "") {
            textCodigoSeguridad.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el código de seguridad", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (codigoSeguridad?.characters.count != 3){
            textCodigoSeguridad.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "El código de seguridad debe de ser de 3 digitos", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        return true
    }
    
}








