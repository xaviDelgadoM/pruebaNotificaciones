//
//  Strings.swift
//  MiExpress
//
//  Created by Jacob Velarde on 28/12/16.
//  Copyright © 2016 JASolutions. All rights reserved.
//

import Foundation
import UIKit

var TITLE_CREATE_ACCOUNT = "Crea tu cuenta";
var TITLE_DELIVERY_ADDRESS = "Dirección de entrega";
var TITLE_HOME = "Categoría";
var TITLE_CONFIRM_SHOP = "Confirma tu compra";
var TITLE_WAY_PAY = "Elige la forma de pago";
var TITLE_DELIVERY_ADDRESS_PAY = "Dirección de entrega";
var TITLE_DETAIL = "Detalle";
var TITLE_LIST_PRODUCT = "Lista";

//Colors

var COLOR_GRAY_TEXTFIELD = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor;
var COLOR_SHARP_BLUE = UIColor(red: 4/255, green: 32/255, blue: 73/255, alpha: 1.0);
var COLOR_GRAY = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0);
var COLOR_LOW_BLUE = UIColor(red: 25/255, green: 140/255, blue: 230/255, alpha: 1.0);
var COLOR_LOW_RED = UIColor(red: 255/255, green: 107/255, blue: 107/255, alpha: 1.0)
var COLOR_LOW_ORANGE = UIColor(red: 255/255, green: 154/255, blue: 73/255, alpha: 1.0)
var COLOR_TRANSPARENT = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)


//Segues
var SEGUE_WAY_PAY_VIEW_CONTROLLER = "WayPayViewController";
var SEGUE_DELIVERY_ADDRESS_PAY_VIEW_CONTROLLER = "DeliveryAddressPayViewController"
var SEGUE_CONFIRM_SHOP_VIEW_CONTROLLER = "ConfirmShopViewController"
var SEGUE_HOME_VIEW_CONTROLLER_FINISH = "HomeViewControllerFinish"
var SEGUE_DELIVERY_ADDRESS_VIEW_CONTROLLER = "DeliveryAddressViewController"
var SEGUE_HOME_VIEW_CONTROLLER_REGISTER = "HomeViewControllerRegister"
var SEGUE_HOME_VIEW_CONTROLLER_LOGIN = "HomeViewControllerLogin"
var SEGUE_PRODUCT_VIEW_CONTROLLER = "ProductViewController"
var SEGUE_PRODUCT_CONTROLLER = "ProductController"
var SEGUE_HOME_VIEW_CONTROLLER = "HomeViewController"
var SEGUE_GENERAL_PRODUCT = "ShowGeneralProduct"
var SEGUE_STORE_CONTROLLER = "TableStoreViewController"

var SEGUE_EXITO = "Exito"

//Identifiers

var CUSTOM_CELL_PRODUCT_IDENTIFIER = "CustomCellProductIdentifier"


//Url request
var URL_PRODUCCION = "http://35.184.6.234/miexpressAPI/api/"
var URL_SERVER_IMAGES = "http://104.154.63.217/webProductosV2/" //Server Images

//Code Error
var EXITO = "200"
var ERROR_DATABASE = "500"
var USER_NOT_FOUND = "300"
var GENERIC_ERROR = "600"
var INTERNET_NOT_AVAILABLE = "Su Conexión a Internet no esta disponible"

//Session
var USER_EMAIL = "user_email"
var EMAIL_NOT_FOUND = "email_not_found"





