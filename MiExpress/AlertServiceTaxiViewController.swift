//
//  AlertServiceTaxiViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 03/06/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

protocol AlertTaxiDelegate {
    func alertClickAceptar();
    func alertClickCancelar();
}

class AlertServiceTaxiViewController: UIViewController {
    
    @IBOutlet weak var txtPrice: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    var alertaDelegate:AlertTaxiDelegate?
    var price = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.masksToBounds = true
        txtPrice.text = "$ "+price
    }
    
    @IBAction func btnClose(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {

            self.dismiss(animated: true, completion: nil)
            self.alertaDelegate?.alertClickCancelar()
            
        })
    }
    
    @IBAction func btnOk(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.dismiss(animated: true, completion: nil)
            self.view.removeFromSuperview()
            self.alertaDelegate?.alertClickAceptar()
        })
    }
    
}
