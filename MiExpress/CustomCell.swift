//
//  CustomCell.swift
//  MiExpress
//
//  Created by Jacob Velarde on 26/01/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var tittle: UILabel!
    @IBOutlet weak var descriptionProduct: UILabel!

}
