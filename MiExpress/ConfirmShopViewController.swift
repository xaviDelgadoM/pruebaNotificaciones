//
//  ConfirmShopViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 08/01/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

class ConfirmShopViewController: BaseViewController{
    
    @IBOutlet weak var labelTotalPay: UILabel!
    @IBOutlet weak var tableProducts: UITableView!
    var data = Session.sharedInstance.getInformationShop()
    @IBOutlet weak var btnBuy: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCustomNavigationBar(title: TITLE_CONFIRM_SHOP)
        self.setIconBackButton()
        self.setCustomBackground()
        //labelTotalPay.layer.addBorder(edge: UIRectEdge.bottom, color: COLOR_LOW_BLUE, thickness: 1)
        tableProducts.delegate = self
        tableProducts.dataSource = self
        getTotalPay()
        
        if data.count <= 0 {
            btnBuy.isEnabled = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = TITLE_CONFIRM_SHOP
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBuy(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_WAY_PAY_VIEW_CONTROLLER, sender: nil)
    }
    
    /*override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == SEGUE_WAY_PAY_VIEW_CONTROLLER){
            if <#condition#> {
                <#code#>
            }
        }
        
        return false;
    }*/
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if (identifier == SEGUE_WAY_PAY_VIEW_CONTROLLER) {
            self.navigationItem.title = ""
            print("hola segue");
        }
    }
    
    public func getTotalPay(){
        var pay = 0
        for i in 0 ..< data.count{
            var information = data[i]
            pay = pay + Int(information["price"]!)!
        }
        Session.sharedInstance.setTotalPay(total: String(pay))
        labelTotalPay.text = "$ " + String(pay)
    }
}

extension ConfirmShopViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableProducts.dequeueReusableCell(withIdentifier: "cellShopProduct") as UITableViewCell!
        let information = data[indexPath.row]

        cell.textLabel?.text = information["nombre"]! + "   $ " + information["price"]!

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            //Update Data
            data.removeAll()
            Session.sharedInstance.removeProduct(index: indexPath.row)
            data = Session.sharedInstance.getInformationShop()
            
            self.tableProducts.reloadData()
            self.getTotalPay()
            
            if data.count <= 0 {
                btnBuy.isEnabled = false
            }
        }
    }
    
}
