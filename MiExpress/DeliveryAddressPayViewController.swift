//
//  DeliveryAddressPayViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 12/01/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit
import CoreLocation

class DeliveryAddressPayViewController : BaseViewController{
    
    @IBOutlet weak var textStreet: UITextField!
    @IBOutlet weak var textNumInt: UITextField!
    @IBOutlet weak var textNumExt: UITextField!
    @IBOutlet weak var textColony: UITextField!
    @IBOutlet weak var textMunicipio: UITextField!
    @IBOutlet weak var textPostalCode: UITextField!
    @IBOutlet weak var textNameEnvio: UITextField!
    
    var locationManager:CLLocationManager!
    var locationUser = [[String : String]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomNavigationBar(title: TITLE_DELIVERY_ADDRESS_PAY)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = TITLE_DELIVERY_ADDRESS_PAY
        determineMyCurrentLocation()
    }
        
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if (identifier == SEGUE_CONFIRM_SHOP_VIEW_CONTROLLER) {
            self.navigationItem.title = ""
        }
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == SEGUE_CONFIRM_SHOP_VIEW_CONTROLLER){
            if validateForm(){
                if self.isInternetAvailable(){
                    self.navigationItem.title = ""
                    self.startActivity()
                    let makeOrder = GenericDelegateResponse()
                    makeOrder.makeRequestPutOrder(calle: textStreet.text!, numInt: textNumInt.text!, numExt: textNumExt.text!, colonia: textColony.text!, municipio: textMunicipio.text!, cp: textPostalCode.text!, ClassDelegate: self)
                    //return true
                }else{
                    self.showMessageWith(text: INTERNET_NOT_AVAILABLE, headerText: "Aviso", actionSegue: "")
                }
            }else{
                return false
            }
        }
        
        return false
    }
    
    private func validateForm() -> Bool{
        let street = textStreet.text
        let num = textNumExt.text
        let colony = textColony.text
        let city = textMunicipio.text
        let cp = textPostalCode.text
        let nameEnvio = textNameEnvio.text
        
        if (nameEnvio == nil || nameEnvio == ""){
            textNameEnvio.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el nombre de la persona que recibirá el envío", headerText: "¡Alerta!", actionSegue: "")
            return false
        }
        
        if (street == nil || street == "") {
            textStreet.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingrese una calle", headerText: "¡Alerta!", actionSegue: "")
            return false
        }
        
        if(num == nil || num == ""){
            textNumExt.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el número de casa exterior", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (colony == nil || colony == ""){
            textColony.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa la colonia", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (city == nil || city == ""){
            textMunicipio.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el municipio", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (cp == nil || cp == ""){
            textPostalCode.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el código postal", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        if (cp?.characters.count != 5){
            textPostalCode.backgroundColor = COLOR_LOW_ORANGE
            self.showMessageWith(text: "Ingresa el código postal correcto", headerText: "¡Alerta!", actionSegue: "")
            return false;
        }
        
        return true;
    }
}

extension DeliveryAddressPayViewController: CLLocationManagerDelegate{
    
    public func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        getLocation(locationManagerL: userLocation)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }

    public func getLocation(locationManagerL : CLLocation){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: locationManagerL.coordinate.latitude, longitude: locationManagerL.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            var locationNameUser = ""
            var streetUser = ""
            var cityUser = ""
            var zipUser = ""
            var countryUser = ""
            //let obj = ["nombre": nombre, "descripcion": descripcion, "imgUrl": imgUrl, "price": price]

            // Print each key-value pair in a new row
            addressDict.forEach { print($0) }
            
            // Print fully formatted address
            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
                print(formattedAddress.joined(separator: ", "))
            }
            
            // Access each element manually
            if let locationName = addressDict["Name"] as? String {
                print(locationName)
                locationNameUser = locationName
            }
            if let street = addressDict["Thoroughfare"] as? String {
                print(street)
                streetUser = street
                self.textStreet.text = street
            }
            if let city = addressDict["City"] as? String {
                print(city)
                cityUser = city
                self.textMunicipio.text = city
            }
            if let zip = addressDict["ZIP"] as? String {
                print(zip)
                zipUser = zip
                self.textPostalCode.text = zip
            }
            if let country = addressDict["Country"] as? String {
                print(country)
                countryUser = country
            }
            
            if let sublicality = addressDict["SubLocality"] as? String{
                print(sublicality)
                self.textColony.text = sublicality
            }
            
            let objectLocation = ["location": locationNameUser, "street": streetUser, "city": cityUser, "zip": zipUser, "country": countryUser]
            
            self.locationUser.append(objectLocation)
            
        })
    }
}

extension DeliveryAddressPayViewController: GenericResponse{
    
    func delegateGenericResponse(codeError: String, message: String, object: String, data: Data) {
        print(codeError)
        print(message)
        print(object)
        print(data)
        
        if codeError == EXITO {
            self.stopActivity()
            //self.showMessageWith(text: "Pedido enviado", headerText: "Aviso", actionSegue: SEGUE_CONFIRM_SHOP_VIEW_CONTROLLER)
            self.nextGenericController(identifier: "ConfirmShopViewControllerXIB")
        }else{
            self.stopActivity()
            self.showMessageWith(text: object, headerText: message, actionSegue: "")
            //self.showMessageWith(text: "Pedido enviado", headerText: "Aviso", actionSegue: SEGUE_CONFIRM_SHOP_VIEW_CONTROLLER)
           //self.nextGenericController(identifier: "ConfirmShopViewControllerXIB")
        }
    }
    
}











