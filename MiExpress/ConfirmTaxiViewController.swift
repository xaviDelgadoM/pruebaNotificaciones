//
//  ConfirmTaxiViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 04/06/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit

class ConfirmTaxiViewController: BaseViewController {
    
    
    @IBOutlet weak var labelFolio: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomNavigationBar(title: "Exito")
        self.hiddenButtonBarLeft(hidden: true)
        
        labelEmail.text = "Correo: "+(UserDefaults.standard.value(forKey: "user_mail") as! String?)!
        labelFolio.text = "Folio: "+Session.sharedInstance.getFolio()

    }
}
