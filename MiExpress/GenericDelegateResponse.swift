//
//  CreateAccountDelegate.swift
//  MiExpress
//
//  Created by Jacob Velarde on 15/02/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol GenericResponse{
    func delegateGenericResponse(codeError:String, message: String, object: String, data: Data)
}

class GenericDelegateResponse:BaseViewController, ServerData {
    
    var delegate:GenericResponse! = nil
    let server = Server.sharedInstance
    
    public func makeRequestCreateAccountWithUser(name: String, email: String, pass: String , phone: String, ClassDelegate: GenericResponse){
        
        delegate=ClassDelegate
        
        let parameters = "name="+name.replacingOccurrences(of: " ", with: "-")+"&contrasena="+pass+"&telefono="+phone+"&correo="+email;
        
        let parametersFalse: [String: Any] = ["data":"data"] //Parametros falsos para permitir hacer la operacion POST
        
        server.makeRequestPOSTWithURL(url: "client?", falseParameters:parametersFalse, parameters: parameters, ClassDelegate: self)
        
    }
    
    public func makeRequestLoginWithUser(email: String, password: String, ClassDelegate: GenericResponse){
        
        delegate = ClassDelegate
        
        let parameters = "correo="+email+"&contrasena="+password;
        
        server.makeRequestGETWithURL(url: "login?", dataParameters: parameters, ClassDelegate: self)
        
    }
    
    public func makeRequestGetProductsWithType(type: String, ClassDelegate: GenericResponse){
        
        delegate = ClassDelegate
        
        let email = Session.sharedInstance.getUserEmail()
        
        if email != EMAIL_NOT_FOUND{
            let parameters = "tipo="+type+"&correo="+email;
            
            server.makeRequestGETWithURL(url: "product?", dataParameters: parameters, ClassDelegate: self)   
        }
    }
    
    public func makeRequestPutStore(category: String, ClassDelegate: GenericResponse){
        
        delegate = ClassDelegate
        
        let email = Session.sharedInstance.getUserEmail()
        
        if email != EMAIL_NOT_FOUND {
            
            let jsonStore:JSON = ["categoria":category,
                                  "correo":email]
            
            server.makeRequestPUTWithURL(url: "tiendaPorCategoria", parameters: jsonStore, ClassDelegate: self)
        }
        
    }

    public func makeRequestPutTaxi(estatus: String, latitudOrigen: Double, longitudOrigen: Double, latitudDestino: Double, longitudDestino: Double, costoServicio: String, classDelegate: GenericResponse){
        
        delegate = classDelegate
        
        let email = Session.sharedInstance.getUserEmail()
        let folio = getFolio()
        Session.sharedInstance.saveFolio(aFolio: folio)

        if email != EMAIL_NOT_FOUND{
            let jsonTaxi:JSON = ["correo":email,
                                 "folio":folio,
                                 "estatus": "2",
                                 "latitudOrigen":latitudOrigen,
                                 "longitudOrigen":longitudOrigen,
                                 "latitudDestino":latitudDestino,
                                 "longitudDestino":longitudDestino,
                                 "costo_servicio":costoServicio]
            server.makeRequestPUTWithURL(url: "taxi", parameters: jsonTaxi, ClassDelegate: self)
        }
        
    }
    
    public func makeRequestPutOrder(calle: String, numInt: String, numExt: String, colonia: String, municipio: String, cp: String, ClassDelegate: GenericResponse){
        
        delegate = ClassDelegate
        
        let email = Session.sharedInstance.getUserEmail()
        let productos = Session.sharedInstance.getInformationShop()
        let totalVenta = Session.sharedInstance.getTotalPay()
        let folio = getFolio()
        Session.sharedInstance.saveFolio(aFolio: folio)
        let status = "2"
        var numInterior = "0"
        var arrayData = [[String: String]]()

        for dictionary:[String:String] in productos {
            print(dictionary)
            let obj = ["idProducto":dictionary["idProducto"],"cantidad":"12"]
            arrayData.append(obj as! [String : String])
        }

        if (numInt.characters.count <= 0){
            numInterior = "0"
        }else{
            numInterior = numInt
        }
        
        if email != EMAIL_NOT_FOUND{
            let jsonOrder:JSON = ["tipo":"envioPedido",
                                  "correo": email,
                                  "calle": calle,
                                  "numInt": numInterior,
                                  "numExt": numExt,
                                  "colonia": colonia,
                                  "municipio":municipio,
                                  "codigoPostal":cp,
                                  "totalVenta":totalVenta,
                                  "folio":folio,
                                  "status":status,
                                  "productos":arrayData]
                        
            server.makeRequestPUTWithURL(url: "order", parameters: jsonOrder, ClassDelegate: self)
        }
    }
    
    public func makeRequestPutProductsByidTienda(idTienda: String, ClassDelegate:GenericResponse){
        delegate = ClassDelegate;
        
        let email = Session.sharedInstance.getUserEmail()
        
        if email != EMAIL_NOT_FOUND{
            let jsonProduct:JSON = ["id_tienda":idTienda,
                                    "correo":email]
            
            server.makeRequestPUTWithURL(url: "productByStore", parameters: jsonProduct, ClassDelegate: self)
        }
        
    }
    
    func serverResponse(codeError: String, message: String, object: String, data: Data) {
        
        delegate.delegateGenericResponse(codeError: codeError, message: message, object: object, data: data)
        
//        if  delegate is CreateAccountViewController{
//            
//            delegate.delegateGenericResponse(codeError: codeError, message: message, object: object, data: data);
//
//        }
//        
//        if delegate is HaveAccountViewController{
//            
//            delegate.delegateGenericResponse(codeError: codeError, message: message, object: object, data: data);
//        }
//        
//        if delegate is HomeViewController{
//            
//            delegate.delegateGenericResponse(codeError: codeError, message: message, object: object, data: data)
//        }
//        
//        if delegate is DeliveryAddressPayViewController{
//            
//            delegate.delegateGenericResponse(codeError: codeError, message: message, object: object, data: data)
//        }
//        
//        if delegate is SelectPositioViewController{
//            
//            delegate.delegateGenericResponse(codeError: codeError, message: message, object: object, data: data)
//        }
        
    }
    
    public func getFolio()->String{
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        return String(hour)+""+String(minutes)+""+String(seconds)+""+String(year)+""+String(month)+""+String(day)
    }
    
}
