//
//  SelectPositionViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 30/05/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class SelectPositioViewController: BaseViewController, MKMapViewDelegate, UIGestureRecognizerDelegate, UISearchControllerDelegate{
    
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    var myLocation:CLLocationCoordinate2D?
    var isCurrent = Bool()
    let annotation = MKPointAnnotation()
    var renderer = MKPolylineRenderer()
    @IBOutlet weak var outletBtnTaxi: UIButton!
    var resultSearchController:UISearchController? = nil
    var locationSearchTable = LocationSearchTableViewController()
    var price = Float()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureLocation()
        self.configureSearchBar()
        
    }
    
    func configureSearchBar(){
        
        locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "SearchTable") as! LocationSearchTableViewController
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Busca un lugar"
        navigationItem.titleView = resultSearchController?.searchBar
        
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
    }
    
    func configureLocation(){
        isCurrent = true
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        mapView.addGestureRecognizer(tap)
        outletBtnTaxi.layer.cornerRadius = 10;
        outletBtnTaxi.layer.masksToBounds = true;
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        
        let location = sender.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
        self.mapView.remove(renderer.overlay)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        //mapView.showsUserLocation = true
        isCurrent = true;
    }
    
    func centerMap(_ center:CLLocationCoordinate2D){
        //self.saveCurrentLocation(center)
        
        let spanX = 0.007
        let spanY = 0.007
        
        let newRegion = MKCoordinateRegion(center:center , span: MKCoordinateSpanMake(spanX, spanY))
        mapView.setRegion(newRegion, animated: true)
    }
    
    func drawRoute(){
        
        if self.isInternetAvailable() {
            
            if(!annotation.coordinate.latitude.isZero){
                let directionRequest = MKDirectionsRequest()
                
                let destinationPlaceMark = MKPlacemark(coordinate: annotation.coordinate, addressDictionary: nil)
                let sourceLocation = CLLocationCoordinate2D(latitude: 19.260892, longitude: -98.896753) //Jardin Central
                let sourcePlaceMark = MKPlacemark(coordinate: sourceLocation,addressDictionary:nil)

                
                let sourceMapItem = MKMapItem(placemark: sourcePlaceMark )
                let destinationMapItem = MKMapItem(placemark: destinationPlaceMark);
                
                directionRequest.source = sourceMapItem
                directionRequest.destination = destinationMapItem
                directionRequest.transportType = .automobile
                
                let directions = MKDirections(request: directionRequest)
                
                self.startActivity()
                directions.calculate {
                    (response, error) -> Void in
                    
                    guard let response = response else {
                        if let error = error {
                            self.showMessageWith(text: error.localizedDescription, headerText: "Aviso", actionSegue: "")
                            self.stopActivity()
                        }
                        
                        return
                    }
                    
                    let route = response.routes[0]
                    print("Distancia: ",route.distance)
                    
                    self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
                    
                    let rect = route.polyline.boundingMapRect
                    self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
                    self.stopActivity()
                    
                    self.price = self.getPrice(distance: Float(route.distance))
                    
                    if (self.price >= 0.0){
                        self.showAlertService(withPrice: String(self.price))
                    }else{
                        self.showMessageWith(text: "Lugar no valido", headerText: "Aviso", actionSegue: "")
                    }
                }
            }else{
                self.showMessageWith(text: "Ingrese un destino", headerText: "Aviso", actionSegue: "")
            }
        }else{
            self.showMessageWith(text: "Necesita una conexión a Internet", headerText: "Aviso", actionSegue: "")
        }
    }
    
    func showAlertService(withPrice: String){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertServiceTaxi") as! AlertServiceTaxiViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.alertaDelegate = self
        myAlert.price = withPrice
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 10

        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    @IBAction func btnGetTaxi(_ sender: Any) {
        definesPresentationContext = false
        drawRoute()
    }
    
    func getPrice(distance: Float)->Float{

        let hourFormat = DateFormatter()
        hourFormat.dateFormat = "HH:mm"
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        let time = (String(hour))+":"+(String(minutes))
        
        let current = hourFormat.date(from: time)
        var currentTime = hourFormat.string(from: current!)
        
        print("La hora es: ",currentTime)
        currentTime = currentTime.replacingOccurrences(of: ":", with: "")
        
        var price = 0;
        
        if (Int(currentTime)! >= 0500 && Int(currentTime)! <= 2259){
            price = 0;
        }else{
            price = 5;
        }
        
        if distance <= 1400 {
            return Float(25+price)
        }else if distance > 1400 && distance <= 2400{
            return Float(30+price)
        }else if distance > 2400 && distance <= 3400{
            return Float(35+price)
        }else if distance > 3400 && distance <= 4400{
            return Float(40+price)
        }else if distance > 4400 && distance <= 5400{
            return Float(45+price)
        }else if distance > 5400 && distance <= 6400{
            return Float(50+price)
        }else if distance > 6400 && distance <= 7400{
            return Float(55+price)
        }else if distance > 7400 && distance <= 8400{
            return Float(60+price)
        }else if distance > 8400 && distance <= 9400{
            return Float(65+price)
        }else if distance > 9400 && distance <= 10400{
            return Float(70+price)
        }else {
            if price == 0{
                return ((distance/1000) * 7.0)
            }else if price == 5{
                return ((distance/1000) * 7.0)
            }
        }
        
        return 0.0
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        self.mapView.remove(renderer.overlay)

        renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        
        return renderer
    }
    
    /*func alertClickAceptar() {
        let confirmTaxiViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmTaxi") as? ConfirmTaxiViewController
        self.navigationController?.pushViewController(confirmTaxiViewController!, animated: true)
    }*/
}

extension SelectPositioViewController : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        myLocation = manager.location!.coordinate
        
        if isCurrent{
            isCurrent = false
            centerMap(myLocation!)
        }
    }
}

extension SelectPositioViewController: AlertTaxiDelegate{
    func alertClickAceptar() {
        
//        let confirmTaxiViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmTaxi") as? ConfirmTaxiViewController
//        self.navigationController?.pushViewController(confirmTaxiViewController!, animated: true)

        if self.isInternetAvailable() {
            self.startActivity()
            let getTaxi = GenericDelegateResponse()
            getTaxi.makeRequestPutTaxi(estatus: "2", latitudOrigen: (myLocation?.latitude)!, longitudOrigen: (myLocation?.longitude)!, latitudDestino: annotation.coordinate.latitude, longitudDestino: annotation.coordinate.longitude, costoServicio: String(self.price), classDelegate: self)
        }else{
            self.showMessageWith(text: INTERNET_NOT_AVAILABLE, headerText: "Aviso", actionSegue: "")
        }
    }
    
    func alertClickCancelar() {
        definesPresentationContext = true
    }
}

extension SelectPositioViewController: HandleMapSearch {
    func dropPinZoomIn(placemark:MKPlacemark){

        mapView.removeAnnotations(mapView.annotations)
        self.mapView.remove(renderer.overlay)

        annotation.coordinate = placemark.coordinate
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView.setRegion(region, animated: true)
    }
}

extension SelectPositioViewController: GenericResponse{
    
    func delegateGenericResponse(codeError: String, message: String, object: String, data: Data) {
        
        if codeError == EXITO{
            self.stopActivity()
            let confirmTaxiViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmTaxi") as? ConfirmTaxiViewController
            self.navigationController?.pushViewController(confirmTaxiViewController!, animated: true)
        } else {
            self.showMessageWith(text: message, headerText: "Aviso", actionSegue: "")
        }
    }
}


