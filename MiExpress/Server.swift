//
//  Server.swift
//  MiExpress
//
//  Created by Jacob Velarde on 15/02/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol ServerData{
    func serverResponse(codeError:String, message:String, object: String, data: Data)
}


class Server {
    
    static let sharedInstance = Server()
    private init() {} //This prevents others from using the default '()' initializer for this class.

    var request: Alamofire.Request?
    
    var delegate:ServerData! = nil

    
    public func makeRequestGETWithURL(url:String, dataParameters:String, ClassDelegate: ServerData){
        
        let urlRequest = URL_PRODUCCION + url + dataParameters
        delegate = ClassDelegate
        
        self.request = Alamofire.request(urlRequest).responseJSON { response in
            
            if response.result.value != nil {
                
                print("JSON: \(response.result.value)")
                print("=====================")
                let jsonResponse = JSON(data: response.data!)
                let codeErrorResponse = jsonResponse["respuesta"]["codeError"].stringValue
                let messageResponse = jsonResponse["respuesta"]["message"].stringValue
                let objectResponse = jsonResponse["respuesta"]["object"].stringValue

                self.delegate.serverResponse(codeError: codeErrorResponse, message: messageResponse, object: objectResponse, data: response.data!)
            }else{
                self.delegate.serverResponse(codeError: "666", message: "Error al cargar información, intente de nuevo", object: "", data: response.data!)
            }
        }
    }
    
    public func makeRequestPOSTWithURL(url:String, falseParameters:[String: Any], parameters:String, ClassDelegate: ServerData){
        let urlRequest = URL_PRODUCCION + url + parameters;
        
        delegate = ClassDelegate
        
        self.request = Alamofire.request(urlRequest, method: .post, parameters: falseParameters, encoding: JSONEncoding.default)
            .responseJSON { response in

                if response.result.value != nil{
                    
                    print("JSON: \(response.result.value)")

                    
                    let jsonResponse = JSON(data: response.data!)
                    let codeErrorResponse = jsonResponse["respuesta"]["codeError"].stringValue
                    let messageResponse = jsonResponse["respuesta"]["message"].stringValue
                    let objectResponse = jsonResponse["respuesta"]["object"].stringValue
                    
                    self.delegate.serverResponse(codeError: codeErrorResponse, message: messageResponse, object: objectResponse, data: response.data!)
                }else{
                    self.delegate.serverResponse(codeError: "666", message: "Error al cargar información, intente de nuevo", object: "", data: response.data!)
                }
        }
    }
    
    public func makeRequestPUTWithURL(url: String, parameters:JSON, ClassDelegate: ServerData){
        
        let urlRequest = URL_PRODUCCION + url
        delegate = ClassDelegate
        let params = parameters.dictionaryObject
        
        self.request = Alamofire.request(urlRequest, method: .put, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                if response.result.value != nil{
                    
                    print("JSON: \(response.result.value)")
                    
                    
                    let jsonResponse = JSON(data: response.data!)
                    let codeErrorResponse = jsonResponse["respuesta"]["codeError"].stringValue
                    let messageResponse = jsonResponse["respuesta"]["message"].stringValue
                    let objectResponse = jsonResponse["respuesta"]["object"].stringValue
                    
                    self.delegate.serverResponse(codeError: codeErrorResponse, message: messageResponse, object: objectResponse, data: response.data!)
                }else{
                    self.delegate.serverResponse(codeError: "666", message: "Error al cargar información, intente de nuevo", object: "", data: response.data!)
                }
        }

        
    }
    
//    func parse(json: JSON) {
//        for result in json["results"].arrayValue {
//            let title = result["title"].stringValue
//            let body = result["body"].stringValue
//            let sigs = result["signatureCount"].stringValue
//            let obj = ["title": title, "body": body, "sigs": sigs]
//            petitions.append(obj)
//        }        
//    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
