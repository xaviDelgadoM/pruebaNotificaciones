//
//  StoresViewController.swift
//  MiExpress
//
//  Created by Jacob Velarde on 15/06/17.
//  Copyright © 2017 JASolutions. All rights reserved.
//

import UIKit
import SwiftyJSON
import SystemConfiguration

class StoresViewController: UITableViewController {
    
    @IBOutlet var tableStore: UITableView!
    var objectData = Data()
    var arrayData = [[String: String]]()
    var activityIndicator = UIActivityIndicatorView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = COLOR_GRAY
        tableStore.delegate = self
        tableStore.dataSource = self
        tableStore.backgroundColor = COLOR_GRAY
        
        parseData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let information = arrayData[indexPath.row]
        
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
            cell?.textLabel?.textColor = .black
            cell?.detailTextLabel?.textColor = .black
            cell?.backgroundColor = COLOR_GRAY
        }
        
        cell?.textLabel?.text = information["nombre"]
        cell?.detailTextLabel?.text = information["sucursal"]
        
        return cell!
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let information = arrayData[indexPath.row]

        makeRequestWithIdTienda(idTienda: information["id_tienda"]!)
    }
    
    func makeRequestWithIdTienda(idTienda:String){
        if self.isInternetAvailable() {
            self.startActivity()
            
            let getProducts = GenericDelegateResponse()
            
            getProducts.makeRequestPutProductsByidTienda(idTienda: idTienda, ClassDelegate: self)
            
        }else{
            self.showMessageWith(text: INTERNET_NOT_AVAILABLE, headerText: "Aviso", actionSegue: "")
        }
    }
    
    func parseData(){
        let jsonResponse = JSON(data: objectData)
        let dataParse = jsonResponse["respuesta"]["object"].stringValue
        
        if let dataFromString = dataParse.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            print(json)
            
            for result in json.arrayValue {
                let nombre = result["nombre"].stringValue
                let sucursal = result["sucursal"].stringValue
                let id_tienda = result["id_tienda"].stringValue
//                let descripcion = result["descripcion"].stringValue
//                let imgUrl = returnStringURL(stringUrl: result["imgUrl"].stringValue)
//                let price = result["precio"].stringValue
//                let obj = ["nombre": nombre, "descripcion": descripcion, "imgUrl": imgUrl, "price": price, "idProducto": idProduct]
                let obj = ["nombre":nombre,"sucursal":sucursal,"id_tienda":id_tienda]
                arrayData.append(obj)
                print(arrayData)
            }
        }
    }
    
    public func isInternetAvailable() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
    
    public func startActivity(){
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.activityIndicator.center = view.center
        self.activityIndicator.layer.backgroundColor = COLOR_TRANSPARENT.cgColor
        self.activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }
    
    public func stopActivity(){
        self.activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
    }
    
    public func showMessageWith(text: String, headerText: String, actionSegue: String){
        let alert = UIAlertController(title: headerText, message: text, preferredStyle: UIAlertControllerStyle.alert)
        let OKAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            
            if actionSegue != ""{
                if self.navigationController != nil {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: actionSegue, sender: nil)
                    }
                }
            }
        }
        
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func nextProductViewController(data: Data){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: SEGUE_PRODUCT_CONTROLLER) as! ProductViewController
        productController.objectData = data
        self.navigationController?.pushViewController(productController, animated: true)
    }
}

extension StoresViewController: GenericResponse{
    func delegateGenericResponse(codeError: String, message: String, object: String, data: Data) {
        self.stopActivity()
        
        if codeError == EXITO {
            self.nextProductViewController(data: data)
        }else{
            self.showMessageWith(text: object, headerText: message, actionSegue: "")
        }
    }
}
